/*
  Note, I started this work with source from: https://mechinations.net/wind-v3/ and https://github.com/guywithaview/peet-bros-wind

  To be compliant with the MIT license that source was distributed with I must keep this notice intact:

  Copyright (c) 2018 Tom K

  Originally written for Arduino Pro Mini 328
  v3c
  MIT License
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.


  HOWEVER, please note this project has now been converted to GPL V2 to be compliant with the RadioHead library:

      anemometer
    Copyright (C) 2018  Ed Welch

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



*/

#include <RH_RF69.h>
#include <RHReliableDatagram.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>
#include "key.h"

#if defined (__AVR_ATmega32U4__) // Feather 32u4 w/Radio
  #define RFM69_CS      8
  #define RFM69_INT     7
  #define RFM69_RST     4
  #define LED           13
#endif

#define VBATPIN A9
#define RANDOM_NOISE A5
#define WIND_SPEED_PIN 0
#define WIND_DIR_PIN 1
#define NOACK 0x01

/************ Radio Setup ***************/

// Change to 915.0 or other frequency, must match RX's freq!
#define RF69_FREQ 915.0

// Where to send packets to!
#define DEST_ADDRESS   1
// change addresses for each client board
#define MY_ADDRESS     4

const unsigned long DEBOUNCE = 10000ul;      // Minimum switch time in microseconds
const unsigned long DIRECTION_OFFSET = 0ul;  // Manual direction offset in degrees, if required
const unsigned long TIMEOUT = 1500000ul;       // Maximum time allowed between speed pulses in microseconds
const unsigned long MIN_UPDATE_RATE = 5000ul;     // What's the minimum update rate to send data (basically when should we force send dupes) in milliseconds
const float filterGain = 0.25;               // Filter gain on direction output filter. Range: 0.0 to 1.0
                                             // 1.0 means no filtering. A smaller number increases the filtering

// MPH is actually stored as (MPH * 100). Deviations below should match these units.
const int BAND_0 =  10 * 100;
const int BAND_1 =  80 * 100;

const int SPEED_DEV_LIMIT_0 =  5 * 100;     // Deviation from last measurement to be valid. Band_0: 0 to 10 MPH
const int SPEED_DEV_LIMIT_1 = 10 * 100;     // Deviation from last measurement to be valid. Band_1: 10 to 80 MPH
const int SPEED_DEV_LIMIT_2 = 30 * 100;     // Deviation from last measurement to be valid. Band_2: 80+ MPH

// Should be larger limits as lower speed, as the direction can change more per speed update
const int DIR_DEV_LIMIT_0 = 25;     // Deviation from last measurement to be valid. Band_0: 0 to 10 MPH
const int DIR_DEV_LIMIT_1 = 18;     // Deviation from last measurement to be valid. Band_1: 10 to 80 MPH
const int DIR_DEV_LIMIT_2 = 10;     // Deviation from last measurement to be valid. Band_2: 80+ MPH

volatile unsigned long speedPulse = 0ul;    // Time capture of speed pulse
volatile unsigned long dirPulse = 0ul;      // Time capture of direction pulse
volatile unsigned long speedTime = 0ul;     // Time between speed pulses (microseconds)
volatile unsigned long directionTime = 0ul; // Time between direction pulses (microseconds)
volatile boolean newData = false;           // New speed pulse received
volatile unsigned long lastUpdate = 0ul;    // Time of last serial output

volatile int mphOut = 0;      // Wind speed output in mph * 100
volatile int prevMphOut = 0;  // The previously sent wind speed in mph
volatile int dirOut = 0;      // Direction output in degrees
volatile boolean ignoreNextReading = false;

float measuredvbat = 0.0;

boolean debug = true;

uint8_t loop_counter = 0;

// Singleton instance of the radio driver
RH_RF69 rf69(RFM69_CS, RFM69_INT);

RHReliableDatagram rf69_manager(rf69, MY_ADDRESS);

// Software SPI Display:
// pin A0 - Serial clock out (SCLK)
// pin A1 - Serial data out (DIN)
// pin A2 - Data/Command select (D/C)
// pin A3 - LCD chip select (CS)
// pin A4 - LCD reset (RST)
Adafruit_PCD8544 display = Adafruit_PCD8544(A0, A1, A2, A3, A4);

void setup()
{
    pinMode(LED, OUTPUT);

    Serial.begin(115200);
    Serial.print("Direction Filter: ");
    Serial.println(filterGain);

    display.begin();
    // init done

    // you can change the contrast around to adapt the display
    // for the best viewing!
    display.setContrast(50);

    display.display(); // show splashscreen
    delay(2000);
    display.clearDisplay();   // clears the screen and buffer


    pinMode(RFM69_RST, OUTPUT);
    digitalWrite(RFM69_RST, LOW);

    // manual reset
    digitalWrite(RFM69_RST, HIGH);
    delay(10);
    digitalWrite(RFM69_RST, LOW);
    delay(10);

    if (!rf69_manager.init()) {
        Serial.println("RFM69 radio init failed");
        while (1);
    }
    Serial.println("RFM69 radio init OK!");
    // Defaults after init are 915.0MHz, modulation GFSK_Rb250Fd250, +13dbM (for low power module)
    // No encryption
    if (!rf69.setFrequency(RF69_FREQ)) {
        Serial.println("setFrequency failed");
    }

    //Change the baudrate to 125k
    rf69.setModemConfig(RH_RF69::GFSK_Rb125Fd125);

    // If you are using a high power RF69 eg RFM69HW, you *must* set a Tx power with the
    // ishighpowermodule flag set like this:
    rf69.setTxPower(20, true);  // range from 14-20 for power, 2nd arg must be true for 69HCW

    // The encryption key has to be the same as the one in the server
    // Create a separate file alongside this one named "key.h" with a 16 byte array uint8_t key[] = { 0x01, 0x02, 0x03....
    rf69.setEncryptionKey(key);



    Serial.print("RFM69 radio @");  Serial.print((int)RF69_FREQ);  Serial.println(" MHz");

    //Seed the random number generator, this is a little goofy, took it from the web, not sure what I think about it yet.
    unsigned long seed = 0;
    for (int i=0; i<32; i++)
    {
        seed = seed | ((analogRead(RANDOM_NOISE) & 0x01) << i);
    }
    randomSeed(seed);

    pinMode(WIND_SPEED_PIN, INPUT);
    attachInterrupt(digitalPinToInterrupt(WIND_SPEED_PIN), readWindSpeed, RISING);

    pinMode(WIND_DIR_PIN, INPUT);
    attachInterrupt(digitalPinToInterrupt(WIND_DIR_PIN), readWindDir, RISING);

    interrupts();


}

void generate_unique_id(byte* packet){
    //Create a random 32bit ID to send with this packet
    for (int i = 0; i < 4; i++)
    {
        packet[i] = (byte) ((((byte)analogRead(RANDOM_NOISE) & 0xFF)) ^ ((byte) random(256)));
    }
}

void readWindSpeed()
{
    // Despite the interrupt being set to RISING edge, double check the pin is now HIGH
    if (((micros() - speedPulse) > DEBOUNCE) && (digitalRead(WIND_SPEED_PIN) == HIGH))
    {
        // Work out time difference between last pulse and now
        speedTime = micros() - speedPulse;

        // Direction pulse should have occured after the last speed pulse
        if (dirPulse - speedPulse >= 0) directionTime = dirPulse - speedPulse;

        newData = true;
        speedPulse = micros();    // Capture time of the new speed pulse
    }
}

void readWindDir()
{
    if (((micros() - dirPulse) > DEBOUNCE) && (digitalRead(WIND_DIR_PIN) == HIGH))
    {
      dirPulse = micros();        // Capture time of direction pulse
    }
}

boolean checkDirDev(long mph, int dev)
{
    if (mph < BAND_0)
    {
        if ((abs(dev) < DIR_DEV_LIMIT_0) || (abs(dev) > 360 - DIR_DEV_LIMIT_0)) return true;
    }
    else if (mph < BAND_1)
    {
        if ((abs(dev) < DIR_DEV_LIMIT_1) || (abs(dev) > 360 - DIR_DEV_LIMIT_1)) return true;
    }
    else
    {
        if ((abs(dev) < DIR_DEV_LIMIT_2) || (abs(dev) > 360 - DIR_DEV_LIMIT_2)) return true;
    }
    return false;
}

boolean checkSpeedDev(long mph, int dev)
{
    if (mph < BAND_0)
    {
        if (abs(dev) < SPEED_DEV_LIMIT_0) return true;
    }
    else if (mph < BAND_1)
    {
        if (abs(dev) < SPEED_DEV_LIMIT_1) return true;
    }
    else
    {
        if (abs(dev) < SPEED_DEV_LIMIT_2) return true;
    }
    return false;
}

void calcWindSpeedAndDir()
{
    unsigned long dirPulse_, speedPulse_;
    unsigned long speedTime_;
    unsigned long directionTime_;
    long windDirection = 0l, rps = 0l, mph = 0l;

    static int prevMph = 0;
    static int prevDir = 0;
    int dev = 0;

    // Get snapshot of data into local variables. Note: an interrupt could trigger here
    noInterrupts();
    dirPulse_ = dirPulse;
    speedPulse_ = speedPulse;
    speedTime_ = speedTime;
    directionTime_ = directionTime;
    interrupts();

    // Make speed zero, if the pulse delay is too long
    if (micros() - speedPulse_ > TIMEOUT) speedTime_ = 0ul;

    // The following converts revolutions per 100 seconds (rps) to mph x 100
    // This calculation follows the Peet Bros. piecemeal calibration data
    // Changing the RPS to be per 100 seconds and the mph * 100 removes a lot of floating point math
    if (speedTime_ > 0)
    {
        rps = 100000000/speedTime_;                  //revolutions per 100s

        if (rps < 323)
        {
          mph = (rps * rps * -11)/10000 + (293 * rps)/100 - 14;
        }
        else if (rps < 5436)
        {
          mph = (rps * rps / 2)/10000 + (220 * rps)/100 + 111;
        }
        else
        {
          mph = (rps * rps * 11)/10000 - (957 * rps)/100 + 32987;
        }
        //knots = mph * 0.86897

        if (mph < 0l) mph = 0l;  // Remove the possibility of negative speed
        // Find deviation from previous value
        dev = (int)mph - prevMph;

        // Only update output if in deviation limit
        if (checkSpeedDev(mph, dev))
        {
          mphOut = mph;

          // If speed data is ok, then continue with direction data
          if (directionTime_ > speedTime_)
          {
              windDirection = 999;    // For debugging only (not output to mph)
          }
          else
          {
            // Calculate direction from captured pulse times
            windDirection = (((directionTime_ * 360) / speedTime_) + DIRECTION_OFFSET) % 360;

            // Find deviation from previous value
            dev = (int)windDirection - prevDir;

            // Check deviation is in range
            if (checkDirDev(mph, dev))
            {
              int delta = ((int)windDirection - dirOut);
              if (delta < -180)
              {
                delta = delta + 360;    // Take the shortest path when filtering
              }
              else if (delta > +180)
              {
                delta = delta - 360;
              }
              // Perform filtering to smooth the direction output
              dirOut = (dirOut + (int)(round(filterGain * delta))) % 360;
              if (dirOut < 0) dirOut = dirOut + 360;
            }
            prevDir = windDirection;
          }
        }
        else
        {
          ignoreNextReading = true;
        }

        prevMph = mph;    // Update, even if outside deviation limit, cause it might be valid!?
    }
    else
    {
        mphOut = 0;
        prevMph = 0;
    }

    if (debug)
    {
        Serial.print(millis());
        Serial.print(",");
        Serial.print(dirOut);
        Serial.print(",");
        Serial.print(windDirection);
        Serial.print(",");
        Serial.println(mphOut/100);
        //Serial.print(",");
        //Serial.print(mph/100);
        //Serial.print(",");
        //Serial.println(rps);
    }
    //If there was a change in speed or we exceed the min update rate, send the output
    int mphOutDiv100 = mphOut/100;
    if ((mphOutDiv100 != prevMphOut) || (millis() - lastUpdate > MIN_UPDATE_RATE))
    {

      //Update the display
      display.clearDisplay();
      display.setTextSize(1);
      display.setTextColor(BLACK);
      display.setCursor(0,0);
      display.print("Speed: ");
      display.println(((float)mphOut)/100);
      display.print("Dir  : ");
      display.println(dirOut);
      display.print("Batt : ");
      display.println(measuredvbat);
      display.print("Retry: ");
      display.println(rf69_manager.retransmissions());
      display.display();

      //Send packet here

      //Initialize the payload
      byte radiopacket[11] = "";
      generate_unique_id(radiopacket);

      Serial.print("ID Bytes: ");
      Serial.print((byte) radiopacket[0]);
      Serial.print(" ");
      Serial.print((byte) radiopacket[1]);
      Serial.print(" ");
      Serial.print((byte) radiopacket[2]);
      Serial.print(" ");
      Serial.println((byte) radiopacket[3]);

      //Sensor ID 201 is Wind Speed SingleByteInt
      radiopacket[4] = 0;
      radiopacket[5] = 201;
      //Break the battery voltage up into one byte for the 1's place
      radiopacket[6] = mphOutDiv100;
      //Sensor ID 202 is Wind Dir TwoByteInt
      radiopacket[7] = 0;
      radiopacket[8] = 202;
      radiopacket[9] = dirOut >> 8;
      radiopacket[10] = dirOut & 0xFF;

      //Set our custom NOACK flag to tell the receiver to not bother sending acks
      rf69_manager.setHeaderFlags(NOACK, RH_FLAGS_NONE);

      rf69_manager.sendto((uint8_t *)radiopacket, 11, DEST_ADDRESS);

      lastUpdate = millis();
      prevMphOut = mphOutDiv100;
    }

}

void blink(byte PIN, byte DELAY_MS, byte loops) {
    for (byte i=0; i<loops; i++)  {
        digitalWrite(PIN,HIGH);
        delay(DELAY_MS);
        digitalWrite(PIN,LOW);
        delay(DELAY_MS);
    }
}

void single_blink(byte PIN, byte DELAY_MS) {
    digitalWrite(PIN,HIGH);
    delay(DELAY_MS);
    digitalWrite(PIN,LOW);
}

void loop()
{
    delay(500);
    loop_counter++;

    //About once a minute send battery voltage
    if (loop_counter >= 120){
        //Read the battery voltage
        measuredvbat = analogRead(VBATPIN);
        measuredvbat *= 2;    // we divided by 2, so multiply back
        measuredvbat *= 3.3;  // Multiply by 3.3V, our reference voltage
        measuredvbat /= 1024; // convert to voltage
        Serial.print("VBat: " ); Serial.println(measuredvbat);

        byte radiopacket[11] = "";
        generate_unique_id(radiopacket);
        //Sensor ID 1 is WS1 Batt Voltage TwoByteFloat
        radiopacket[4] = 0;
        radiopacket[5] = 200;
        //Break the battery voltage up into one byte for the 1's place
        radiopacket[6] = (byte) measuredvbat;
        //And 1 byte for the tenths and hundredths
        radiopacket[7] = (byte)((measuredvbat - radiopacket[6]) * 100);

        //Also send back an indicator of retransmissions
        //Sensor ID 11 is WS1 retransmissions SingleByteInt
        radiopacket[8] = 0;
        radiopacket[9] = 203;
        radiopacket[10] = (byte) rf69_manager.retransmissions();
        //Reset the counter
        rf69_manager.resetRetransmissions();

        //Clear our NOACK flag so that we get an ack for this message
        rf69_manager.setHeaderFlags(RH_FLAGS_NONE, NOACK);

        // Send a message to the DESTINATION!
        if (rf69_manager.sendtoWait((uint8_t *)radiopacket, 11, DEST_ADDRESS)) {
            //Blink if we got an ACK
            blink(LED, 40, 4);
        } else {
            Serial.println("Sending failed (no ack)");
            blink(LED, 20, 8);
        }

        loop_counter=0;
    }

    //Heartbeat and wind measurement
    if(loop_counter % 2 == 0){
        Serial.println(loop_counter);
        single_blink(LED, 50);
        calcWindSpeedAndDir();
    }

}

